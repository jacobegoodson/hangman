package java_src;

import clojure.lang.Keyword;
import clojure.lang.PersistentVector;
import clojure.lang.Symbol;

public class CoRoutine {
    private Continuation continuation;
    private ContinuationScope scope;
    private Runnable fn;
    private Object val;
    private boolean done;
    private long finishedTime = 0;

    public void setContinuation(Continuation continuation) {
        this.continuation = continuation;
    }

    public void setScope(ContinuationScope scope) {
        this.scope = scope;
    }

    public Runnable getFn() {
        return fn;
    }

    public void setFn(Runnable fn) {
        this.fn = fn;
    }

    public static void configureCoroutine(CoRoutine co, ContinuationScope scope, Runnable runnable){
        co.setFn(runnable);
        co.setContinuation(new Continuation(scope,runnable));
        co.setScope(scope);
        co.setDone(false);
        co.setVal(null);
    }

    public void setVal(Object val) {
        this.val = val;
    }

    public boolean isDone() {
        return done;
    }
    public boolean isNotDone() {
        return !done;
    }

    public Continuation getContinuation() {
        return continuation;
    }

    public ContinuationScope getScope(){
        return scope;
    }

    private PersistentVector innerRun(){
        if(!isDone()){
            continuation.run();
            if (isDone()){
                setFinishedTime();
                return PersistentVector.create(getVal(), Keyword.intern(Symbol.create("done")));
            } else{
                setFinishedTime();
                return PersistentVector.create(getVal(), Keyword.intern(Symbol.create("not-done")));
            }
        }
        return PersistentVector.create(getVal(), Keyword.intern(Symbol.create("done")));
    }

    public PersistentVector run(){
        return innerRun();
    }

    public Object getVal() {
        return val;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public long getFinishedTime() {
        return finishedTime;
    }

    public void setFinishedTime() {
        this.finishedTime = System.nanoTime();
    }
    public void setFinishedTime(long time) {
        this.finishedTime = time;
    }

    public static void main(String[] args) {

    }
}
