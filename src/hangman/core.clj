(ns hangman.core
  (:import (java.lang Continuation ContinuationScope)
           (java_src CoRoutine MutableData)))

(defprotocol Run
  (run [^CoRoutine this]
    [^CoRoutine this arg-one]))

(extend-type CoRoutine
  Run
  (run [^CoRoutine this]
    (.run this)))
;(run [^CoRoutine this arg-one]
; (.run this arg-one)))

;; TODO possibly remove this macro
(defmacro do-while
  [test & body]
  `(loop []
     ~@body
     (when ~test
       (recur))))

;; place holders for code complete =)
;; these transform in the body of gen-continuation using anaphoric macro expansion

(def ^{:doc
       " yield has two cases...
         yield with no value:
           (yield) => (Continuation/yield scope)
         yield with a value:
           (yield 1) => (do (.setVal coroutine 1) (Continuation/yield scope))
       "}
  yield)

;; this transforms into the current coroutine you are working in
(def ^{:doc
       "transforms into the current coroutine you are working in...
        (cont some-scope (do some things) <this>)
        becomes...
        (cont some-scope (do some things) <cont>)
        referring back to the continuation itself
       "}
  this)

;; wait will wait for a specified amount of time in microseconds until continuing
;; TODO finish wait!
(def wait)

; a default scope to work with
(def scope (ContinuationScope. "default"))

(defn- replace-bind
  [binder args code]
  (let [groups (partition 2 args)
        bindings (map (fn [[arg val]]
                        [arg (gensym arg) val])
                      groups)
        binding-replace (reduce (fn [acc [arg sym _]]
                                  (assoc acc arg sym))
                                {}
                                bindings)
        bindings (into [] (flatten (map (fn [[_ arg val]]
                                          [arg val])
                                        bindings)))]
    `(~binder ~bindings ~@(clojure.walk/postwalk (fn [c]
                                                   (if (binding-replace c)
                                                     (binding-replace c)
                                                     c))
                                                 code))))

(defn- yield-call?
  [code]
  (and (list? code)
       (or (= (first code) 'yield)
           (= (first code) 'games-with-clojure.hangman/yield))
       (not= (count code) 2)))

(defn- yield-call
  [co scope]
  `(do (.setVal ~co nil)
       (Continuation/yield ~scope)
       nil))

(defn- yield-call-return?
  [code]
  (and (seq? code)
       (or (= (first code) 'yield)
           (= (first code) 'games-with-clojure.hangman/yield))
       (= (count code) 2)))

(defn- yield-call-return
  [co scope code]
  `(do (.setVal ~co ~(last code))
       (Continuation/yield ~scope)
       (.getVal ~co)))

(defn- let-call?
  [code]
  (and (seq? code)
       (= (first code) 'let)))

(defn- dotimes-call?
  [code]
  (and (seq? code)
       (= (first code) 'dotimes)))

(defn- dotimes-call
  [code]
  (let [[_ args & code] code]
    (replace-bind 'dotimes args code)))


(defn- let-call
  [[_ args & code]]
  (let [format (replace-bind 'let args code)
        [_ args & code] format
        groups (partition 2 args)
        vars (into #{} (map first groups))
        args (reduce (fn [acc [var val]]
                       (conj acc var `(MutableData. ~val)))
                     []
                     groups)]
    `(let ~args ~@(clojure.walk/postwalk (fn [c]
                                           (cond (and (seq? c)
                                                      (not= (first c) 'set))
                                                 (map (fn [item]
                                                        (if (contains? vars item)
                                                          `(.getData ~item)
                                                          item))
                                                      c)
                                                 (and (seq? c)
                                                      (= (first c) 'set))
                                                 `(.setData ~(second c) ~(last c))
                                                 :else c))
                                         code))))



(defn wait-call?
  [code]
  (and (seq? code)
       (= (first code) 'wait)
       (= (count code) 1)))

(defn wait-call
  [co scope]
  `(do
     (.setFinishedTime ~co)
     (Continuation/yield ~scope)))

(defn wait-call-duration?
  [code]
  (and (seq? code)
       (= (first code) 'wait)
       (= (count code) 2)))

(defn wait-call-duration
  [co scope [_ duration]]
  `(let [time-to-wait# (+ ~(* duration 1000000) (System/nanoTime))]
     (while (> time-to-wait# (System/nanoTime))
       (.setFinishedTime ~co)
       (Continuation/yield ~scope))))


(defn- co-routine-transformer
  [co scope code]
  (clojure.walk/postwalk (fn [code]
                           (cond (wait-call? code)             (wait-call co scope)
                                 (dotimes-call? code)          (dotimes-call code)
                                 (wait-call-duration? code)    (wait-call-duration co scope code)
                                 (let-call? code)              (let-call code)
                                 (yield-call-return? code)     (yield-call-return co scope code)
                                 (yield-call? code)            (yield-call co scope)
                                 :else                         code))
                         code))



(defmacro cont
  [scope & body]
  (let [co (gensym)
        transformed (co-routine-transformer co scope body)]
    `(let [~co (CoRoutine.)
           ~(identity 'this) ~co]
       (CoRoutine/configureCoroutine ~co
                                     ~scope
                                     (fn []
                                       ~@(concat (butlast transformed)
                                                 `((do (.setVal ~co ~(last transformed))
                                                       (.setDone ~co true))))))
       ~co)))

(defn- gensymed
  [coroutines]
  (reduce (fn [acc x] (conj acc (gensym) x)) [] coroutines))

(defn- extract-coroutines
  [gensymed]
  (map first (partition 2 gensymed)))

(defn- race-sync-body
  [condition? scope coroutines]
  (clojure.walk/macroexpand-all `(cont ~scope
                                       (let [inner-race# ~(clojure.walk/macroexpand-all
                                                            `(cont ~scope
                                                                   (while (do (.setVal ~(identity 'this) ~(mapv (fn [x] `(first (.run ~x))) coroutines))
                                                                              (~condition? (fn [x#]
                                                                                               (.isDone x#))
                                                                                        ~(vec coroutines)))
                                                                     (~(identity 'yield) (.getVal ~(identity 'this))))
                                                                   (.getVal ~(identity 'this))))]
                                         (while (not (and (.run inner-race#)
                                                          (.isDone inner-race#)))
                                           (~(identity 'yield) (.getVal inner-race#)))
                                         (mapv (fn [x#] (.getVal x#)) ~(vec coroutines))))))

(defmacro sync
  [scope & coroutines]
  (let [gensymed (gensymed coroutines)
        coroutines (extract-coroutines gensymed)]
    `(let ~gensymed
       ~(race-sync-body 'not-every? scope coroutines))))


(defmacro race
  [scope & coroutines]
  (let [gensymed (gensymed coroutines)
        coroutines (extract-coroutines gensymed)]
    `(let ~gensymed
       ~(race-sync-body 'not-any? scope coroutines))))


;; TEST CODE ;;
(do
  (def race-1 (cont scope (println "yo yall") (yield 1) nil))

  (def race-2 (cont scope (dotimes [x 10] (println "hello") (yield x))))
  (def multi
    (race scope
          race-1
          race-2
          (cont scope (println "hello") (yield "third") (yield 4)))))
(run multi)
(do
  (def sync-1 (cont scope (println "yo yall") (yield "second")))

  (def sync-2 (cont scope (dotimes [x 5] (println x) (yield x)) "third"))
  (def multi
    (sync scope
          sync-1
          sync-2
          (cont scope "hello" "first"))))

(run multi)





